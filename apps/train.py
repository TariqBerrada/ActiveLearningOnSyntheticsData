import joblib

import matplotlib.pyplot as plt
import torch
from torch.utils.data import DataLoader

from module.MLPClassifier import MLPClassifier
from utils.dataloader import DatasetClass, split_ds
from utils.trainer import fit

if __name__ == "__main__":
    # Configs
    lr = 5e-3
    n_epochs = 600
    batch_size_train = 256
    batch_size_val = 500

    active = True
    thresh = 0.7
    log_freq = 50
    save_dir = '' # None or '' to not save


    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    plt.style.use('bmh')

    data = joblib.load('data/dataset_init.pt')
    data_train, data_val = split_ds(data, perc = .75) # dict with keys 'x', 'y'
    train_loader= DataLoader(DatasetClass(data_train),
                             batch_size=batch_size_train,
                             shuffle=True)
    val_loader = DataLoader(DatasetClass(data_val),
                            batch_size=batch_size_val,
                            shuffle=True)

    model = MLPClassifier(input_dim=2, output_dim=3, device=device).to(device)
    optimizer = torch.optim.Adam(model.parameters(), lr=lr)
    criterion = torch.nn.CrossEntropyLoss(reduction='none')

    fit(model, train_loader, val_loader, optimizer, criterion, active=active,
        thresh=thresh, n_epochs=n_epochs, log_freq=log_freq, save_dir=save_dir)
