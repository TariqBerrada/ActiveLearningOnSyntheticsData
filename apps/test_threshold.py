import joblib
import torch

from module.MLPClassifier import MLPClassifier
from utils.dataloader import DatasetClass
from utils.active_learning.selector import thresh_proba, thresh_gradient

if __name__ == '__main__':
    # Configs
    # selector = thresh_proba
    # thresh_list = [0.999, 0.99, 0.95, 0.9, 0.8, 0.7, 0.6]
    selector = thresh_gradient
    thresh_list = [1e-4, 1e-3, 2e-3, 3e-3]

    # Load a saved model and data
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    model = MLPClassifier(input_dim=2, output_dim=3, device=device).to(device)
    model.eval()
    data = joblib.load('data/dataset_init.pt')
    data_loader = torch.utils.data.DataLoader(DatasetClass(data),
                                              batch_size=64)

    criterion = torch.nn.CrossEntropyLoss(reduction='none')

    n = data['x'].shape[0]
    print('number of data:', n, '\n')
    for thresh in thresh_list:
        selected_data = selector(model=model, loader=data_loader,
                                 thresh=thresh, criterion=criterion)
        n_select = selected_data.shape[0]
        print(f'Thresh {thresh}, selected data: {n_select} '
              f'(prop {100 * n_select / n :.3f}%)')
