# ActiveLearningOnSyntheticsData
This repository presents the accompanying code for our *"Real Fluids Modelling using Active Learning"* project.

The code is organized in different notebooks, corresponding to different parts of the article :

* `tuto/ActiveTut.ipynb` : Contains the code for the active learning strategies developped for the classification task (sections 3 and 4).
* `tuto/regresion_example.ipynb` : Contains the code for the regression on the image completion problem (section 5).
* `tuto/thermo_regression.ipynb` : Contains the application on the thermodynamic datasets (section 6).