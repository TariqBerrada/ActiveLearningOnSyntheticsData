import warnings
warnings.filterwarnings("ignore")

import torch
from torch.utils.data import DataLoader
import tqdm

from utils.dataloader import DatasetClass, augment_until

def fit(model, train_loader, val_loader, optimizer, criterion, active=True,
          thresh=.7, n_epochs=200, log_freq=50, save_dir='', start_active=50):
    """ Perform a trainig and a validation loop on the model.
    Parameters
    ----------
    model : torch.nn.Module
    train_loader : torch.utils.data.DataLoader
    val_loader : torch.utils.data.DataLoader
    optimizer : torch.optim.Optimizer
    criterion : torch.nn.Module
        The loss function.
    active : bool
        Whether to use the active learning strategy. Interesting
        data are stored at each batch (for data visualization).
    thresh : float
        The threshold for the active learning strategy
        (unused if active is False).
    n_epochs : int
    log_freq : int
        The number of epochs between each log on the console.
    save_dir: str, optional
        The path to the directory where to save the model.
        If '' or None, no model is saved. By default ''.

    Returns
    -------
    metrics : dict
        Metrics of the training (at each epoch).
    """

    metrics = {'L_train' : [], 'L_val' : []}
    # tqdm_iterator = tqdm.tqdm(range(n_epochs))
    tqdm_iterator = range(n_epochs)
    for i in tqdm_iterator:
        train_loss, failure_modes = train(
            model=model, loader=train_loader, optimizer=optimizer,
            criterion=criterion, active=active, thresh=thresh
            )
        # Start using the active learning strategy after 50 epoch
        if active and i > start_active:
            augmented = augment_until(failure_modes, 300)
            acl_loader = DataLoader(DatasetClass(augmented), batch_size=64)
            _, _ = train(model, acl_loader, optimizer, criterion, active,
                         thresh)

        with torch.no_grad():
            val_loss = validate(model, val_loader, criterion)

        metrics['L_train'].append(train_loss)
        metrics['L_val'].append(val_loss)

        # if (i+1) % log_freq == 0:
        #     # tqdm_iterator.write(
        #     print(  
        #         f'Epoch {i+1}: ',
        #         f'L_train : {train_loss: .6f} - ',
        #         f'L_val : {val_loss: .6f}'
        #         )

        if save_dir is not None and save_dir != '':
            torch.save(model.state_dict(), save_dir)
    # print('Done training.')
    return metrics


def train(model, loader, optimizer, criterion, active, thresh):
    model.train()
    running_loss = .0
    failure_modes = {'x':[], 'y':[]}
    for batch in loader:
        optimizer.zero_grad()

        x = batch['x'].float().to(model.device)
        target = batch['y'].float().to(model.device)
        _, target = target.max(dim=-1)
        target = target.type(torch.long)

        pred = model(x)
        if active:
            errors = torch.abs(1 - pred[[u for u in range(target.shape[0])],
                                        target])
            failure_modes['x'].append(batch['x'][errors > thresh,:])
            failure_modes['y'].append(batch['y'][errors > thresh,:])

        loss = criterion(pred, target)

        loss.backward()
        optimizer.step()

        running_loss += loss.item() / len(loader.dataset)
    if active:
        for k, v in failure_modes.items():
            failure_modes[k] = torch.cat(v)

    return running_loss, failure_modes


def validate(model, loader, criterion):
    model.eval()
    running_loss = .0
    with torch.no_grad():
        for batch in loader:
            x = batch['x'].float().to(model.device)
            target = batch['y'].float().to(model.device)
            _, target = target.max(dim=-1)
            pred = model(x)

            loss = criterion(pred, target).mean()
            running_loss += loss.item()
    return running_loss/len(loader.dataset)
