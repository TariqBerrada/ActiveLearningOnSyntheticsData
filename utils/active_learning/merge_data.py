import torch

def classic_merge(old_dataset, new_dataset, n_remain):
    n = old_dataset['x'].shape[0]
    idx = torch.randperm(n)[: n_remain]
    X = torch.cat((old_dataset['x'][idx], new_dataset['x']), dim=0)
    Y = torch.cat((old_dataset['y'][idx], new_dataset['y']), dim=0)
    return {'x': X, 'y': Y}
