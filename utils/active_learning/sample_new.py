import torch

def normal_with_std(selected_data, num_samples, std):
    selected_data = selected_data.float()
    rd_perm = torch.randperm(selected_data.size(0))
    selected_data = selected_data[rd_perm][:num_samples]
    return torch.normal(selected_data, std)


def normal_neighbor(selected_data, all_data, ratio_std_dist=0.5,
                    num_samples=None):
    """ Sample new data from the neighborhood of the selected data
    with an adaptative std (depending on the distance to the
    nearest neighbor).

    Parameters
    ----------
    selected_data : torch.tensor
        Data to sample around.
    all_data : torch.tensor
        All data.
    ratio_std_dist : float, optional
        Ratio between the std to use for normal sampling and the
        distance to the nearest neighboor. By default 0.5.
    num_samples : int or None, optional
        Number of new data to sample. If None, all new data will be
        selected. By default None.

    Returns
    -------
    torch.tensor
        New data sampled (but not labeled).
    """
    selected_data = selected_data.float()
    all_data = all_data.float()
    if num_samples is None or num_samples > len(selected_data):
        num_samples = len(selected_data)
    # Get num_samples random samples from selected_data
    idx = torch.randperm(len(selected_data))
    selected_data = selected_data[idx][: num_samples]

    # Get the closest sample in all_data in the rest of the data
    selected_data_unsqueeze = torch.unsqueeze(selected_data, dim=1)
    all_data_unsqueeze = torch.unsqueeze(all_data, dim=0)
    diff = selected_data_unsqueeze - all_data_unsqueeze
    dist = torch.norm(diff, p=2, dim=-1)
    # Avoid to select the same sample
    dist = torch.where(dist < 1e-6, dist.max() + 1, dist)

    neighbors_idx = torch.argmin(dist, dim=1)
    dist_neighbors = torch.norm(selected_data - all_data[neighbors_idx], p=2,
                                dim=-1)
    dist_neighbors = torch.unsqueeze(dist_neighbors, dim=1)
    rd_vec = torch.rand((num_samples, 1)).to(selected_data.device)

    return selected_data + ratio_std_dist * rd_vec * dist_neighbors


if __name__ == '__main__':
    selected_data = torch.tensor([[0, 1, 0], [0, 1, 0]])
    all_data = torch.tensor([[0, 1, 0], [1, 0, 1], [1, 1, 1]]) + 0.1
    new_data = normal_neighbor(selected_data, all_data, 0.01)
    print(new_data)  # all rows are close to [[0, 1, 0]]
