import torch

from module.MLPClassifier import MLPClassifier
from utils.dataloader import DatasetClass

def thresh_proba(model, loader, thresh, **kwargs):
    model.eval()
    with torch.no_grad():
        selected_data = []
        for batch in loader:
            x = batch['x'].float().to(model.device)
            pred = torch.nn.Softmax(dim=-1)(model(x))
            pred_best_class = torch.max(pred, dim=-1)[0]
            idx_select = 1 - pred_best_class > 1 - thresh
            selected_data.append(x[idx_select])
        selected_data = torch.cat(selected_data, dim=0)
    return selected_data


def thresh_gradient(model, loader, thresh, criterion, **kwargs):
    n_params = 0
    for param in model.parameters():
        n_params += param.numel()
    optimizer = torch.optim.SGD(model.parameters(), lr=1.0)
    model.eval()
    selected_data = []
    for batch in loader:
        # Pass forward sample by sample to get per-sample gradient
        for x, y in zip(batch['x'], batch['y']):
            optimizer.zero_grad()
            x = torch.unsqueeze(x.float().to(model.device), dim=0)
            target = torch.unsqueeze(y.float().to(model.device), dim=0)
            _, target = target.max(dim=-1)
            pred = model(x)
            loss = criterion(pred, target)
            loss.backward()
            # Get gradient
            square_norm = 0.0
            for param in model.parameters():
                square_norm += torch.sum(param.grad ** 2)
            mean_grad_norm = torch.sqrt(square_norm) / n_params
            if mean_grad_norm > thresh:
                selected_data.append(x)
    selected_data = torch.cat(selected_data, dim=0)
    return selected_data

if __name__ == '__main__':
    from matplotlib import pyplot as plt
    import numpy as np

    from utils.generate_data import xmin, xmax, ymin, ymax
    from utils.get_labels import label_pts

    selector = thresh_gradient

    # Load a saved model & data
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    model = MLPClassifier(input_dim=2, output_dim=3, device=device).to(device)
    model.load_state_dict(torch.load('saved_model/ckpt.pth'))

    cloud = np.random.uniform(low=0, high = 1, size = (20000, 2))
    cloud[:, 0] = cloud[:, 0]*(xmax - xmin) + xmin
    cloud[:, 1] = cloud[:, 1]*(ymax - ymin) + ymin

    cloud_pred = model(torch.tensor(cloud).float().to(model.device))
    cloud_pred = torch.nn.Softmax(dim=-1)(cloud_pred).cpu().detach().numpy()
    cloud_labels = label_pts(cloud)
    data = {'x': torch.tensor(cloud), 'y': torch.tensor(cloud_labels)}
    data_loader = torch.utils.data.DataLoader(DatasetClass(data),
                                              batch_size=64)

    criterion = torch.nn.CrossEntropyLoss(reduction='none')

    fig, ax = plt.subplots(1, 3, figsize = (24, 8))

    ax[0].scatter(cloud[:, 0], cloud[:, 1], c=cloud_labels, s=10, alpha=.4)
    ax[0].set_title('ground truth')

    ax[1].scatter(cloud[:, 0], cloud[:, 1], c=cloud_pred, s=10, alpha=.4)
    ax[1].set_title('predictions')

    ax[2].scatter(cloud[:, 0], cloud[:, 1], c=cloud_pred, s=10, alpha=.4)
    ax[2].set_title('predictions & selection')

    # NOTE: remove 'criterion' for thresh_proba
    selected_data = selector(model, criterion, data_loader, 3e-3)
    selected_data = selected_data.cpu().detach().numpy()
    ax[2].scatter(selected_data[:, 0], selected_data[:, 1], marker = 's',
                  c='black', s=10, alpha=1, label='selected data')
    plt.legend(loc='best')
    plt.show()
